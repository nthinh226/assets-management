# Docker images can be inherited from other images
FROM node:latest
WORKDIR /app
# Copy from host to image
COPY ./ ./
# Run command in the image
RUN npm install
# Run inside a container
CMD ["npm","start"]
