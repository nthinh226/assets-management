https://stackedit.io/app#
# How to preview makedown file
  Ctrl + Shift + V
# How to use Jest in ES6
## Cannot use import statement outside a module 
1. Go to: https://babeljs.io/setup#installation
2. Create file .babelrc in root folder
3. Paste:
```
{
  "presets": ["@babel/preset-env"]
}
```
to .babelrc already created
4. Go to: https://babeljs.io/setup#installation
5. install: npm install @babel/preset-env --save-dev

## Auto reload
add *--watch* in script in package.json
<br>
ex:
```
"scripts": {
    "start": "node src/index.js",
    "dev": "env-cmd -f ./config/dev.env nodemon src/index.js",
    "test": "jest --watch"
}
```

## Asynchronous
1. Add parameter for function
```
test('Asynchronous test demo', (done) => {
	setTimeout(() => {
		done()
	}, 2000)
});
```
2. Use promise
```
test('Should add two numbers', (done) => {
	add(2, 3).then((sum) => {
		expect(sum).toBe(5);
		done();
	});
});
```
3. Use async/await
```
test('Should add two numbers async/await', async () => {
    const sum = await add(10, 22);
    expect(sum).toBe(32);
});
```
# esbuild
## Install esbuild
> npm install esbuild

You can run the esbuild executable to verify that everything is working correctly:

Unix:
> ./node_modules/.bin/esbuild --version

Windows:
> .\node_modules\.bin\esbuild --version