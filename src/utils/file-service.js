import multer from 'multer';
import path from 'path'

const memoryStorage = multer.memoryStorage();
var diskStorage = multer.diskStorage({
	destination: function (req, file, cb) {
	  cb(null, 'public/images')
	},
	filename: function (req, file, cb) {
	  cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
	}
  })
// const uploadPhoto = multer({
// 	storage,
// 	limits: {
// 		fileSize: 1000000,
// 	},
// 	fileFilter(req, file, callback) {
// 		if (!file.originalname.match(/\.(jpg|jpeg|png)$/)) {
// 			return cb(new Error('Please upload an image'));
// 		}
// 		callback(undefined, true);
// 	},
// });

const uploadPhoto = multer({
	storage: memoryStorage,
	limits: {
		fileSize: 1000000,
	},
	fileFilter(req, file, callback) {
		if (!file.originalname.match(/\.(jpg|jpeg|png)$/)) {
			return cb(new Error('Please upload an image'));
		}
		callback(undefined, true);
	},
});
const uploadImage = multer({
	storage: diskStorage,
	limits: {
		fileSize: 10000000,
	},
	fileFilter(req, file, callback) {
		if (!file.originalname.match(/\.(jpg|jpeg|png)$/)) {
			return cb(new Error('Please upload an image'));
		}
		callback(undefined, true);
	},
});

export {
	uploadPhoto,
	uploadImage,
}