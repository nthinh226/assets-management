import chai from 'chai';
import chaiHttp from 'chai-http';
import { app } from '../index.js';
import { User } from '../models/user.js';
import mongoose from 'mongoose';
let should = chai.should();

const userOneId = new mongoose.Types.ObjectId();
const userOne = {
	_id: userOneId,
	firstName: 'Test1',
	lastName: 'Case',
	birthDate: '2001/02/26',
	username: 'ng22',
	email: 'ngocthinh1126@gmail.com',
	password: 'A@12345',
};

chai.use(chaiHttp);
//Our parent block
describe('Users', () => {
	beforeEach((done) => {
		//Before each test we empty the database in your case
		const deleteUser = User.deleteMany()
		const createUser = new User(userOne).save()
		Promise.all([deleteUser, createUser]).then(()=>{
			done()
		})
	});
	/*
	 * Test the /POST route
	 */
	describe('/POST users', () => {
		it('it should POST a user', (done) => {
			const user = {
				firstName: 'Thinh',
				lastName: 'Tran',
				birthDate: '2001/02/26',
				username: 'ng22',
				email: 'ngocthinh1126@gmail.com',
				password: 'A@12345',
			};
			chai.request(app)
				.post('/users')
				.send(user)
				.end((err, res) => {
					res.should.have.status(201);
					res.body.should.be.a('object');
					res.body.user.should.have.property('_id');
					res.body.user.should.have
						.property('firstName')
						.eql(user.firstName);
					res.body.user.should.have
						.property('lastName')
						.eql(user.lastName);
					res.body.user.should.have
						.property('username')
						.eql(user.username);
					res.body.user.should.have.property('email').eql(user.email);
					done();
				});
		});
		it('it should not POST a user without status field', (done) => {
			let user = {
				firstName: '',
				lastName: 'Tran',
				birthDate: '2001/02/26',
				username: 'ng22',
				email: 'ngocthinh1126@gmail.com',
				password: 'ab',
			};
			chai.request(app)
				.post('/users')
				.send(user)
				.end((err, res) => {
					res.should.have.status(406);
					res.body.should.be.a('object');
					done();
				});
		});
		it('it should login success', (done) => {
			let user = {
				email: 'ngocthinh1126@gmail.com',
				password: 'A@12345',
			};
			chai.request(app)
				.post('/users/login')
				.send(user)
				.end((err, res) => {
					res.should.have.status(200);
					done();
				});
		});
	});
		/*
	 * Test the /GET route
	 */
		describe('/GET users', () => {
			it('it should GET all the users', (done) => {
				chai.request(app)
					.get('/users')
					.end((err, res) => {
						res.should.have.status(200);
						res.body.data.should.be.a('array');
						// res.body.length.should.be.eql(1); // fixme :)
						done();
					});
			});
		});
});
