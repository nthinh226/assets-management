import sgMail from '@sendgrid/mail';

sgMail.setApiKey(process.env.SENDGRID_API_KEY);

export const sendResetPasswordEmail = (email, token) => {
	sgMail.send({
		to: email,
		from: 'ngocthinh1126@gmail.com',
		subject: 'Password Reset',
		html: `<h2>Please click on given link to reset your password account</h2>
                    <div>localhost:3000/users/reset-password/${token}</div>`,
	});
};
