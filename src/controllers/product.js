import { Product } from '../models/product.js';

const uploadProduct = async (req, res) => {
	const fileName = req.file.filename;
	res.status(200).send(fileName);
};
const createProduct = async (req, res) => {
	try {
		const existingProduct = await Product.findOne({
			name: req.body.name,
		}).lean(true);
		if (existingProduct) {
			return res.status(403).send('Product already exists.');
		} else {
			const product = await Product.create({
				name: req.body.name,
				brand: req.body.brand,
				description: req.body.description,
				quantity: req.body.quantity,
				category: req.body.category,
				approvalDate: req.body.approvalDate,
			});
			if (product) {
				return res.status(201).send(product);
			} else {
				res.status(403).send({
					message: 'Product has not been created.',
				});
			}
		}
	} catch (error) {
		console.log(error);
		res.status(400).send({
			message: error.message,
		});
	}
};

const getProduct = async (req, res) => {
	const limit = req.body.limit;
	const page = req.body.page;
	const skip = (page - 1) * limit;
	const totalItems = await Product.countDocuments({});
	const totalPages_pre = parseInt(totalItems / limit);
	const totalPages =
		total_items % limit == 0 ? totalPages_pre : totalPages_pre + 1;

	try {
		const product = await Product.find({}).limit(limit).skip(skip).sort({});
		res.send({
			totalPages,
			currentPage: page,
			product,
		});
	} catch (e) {
		res.status(500).send({ message: e.message });
	}
}

const getProductId = async (req, res) => {
	const _id = req.params.id;

	try {
		const product = await Product.findById(_id);
		if (!product) {
			res.status(404).send({ message: 'Product does not exist.' });
		}
		res.send(product);
	} catch (e) {
		res.status(500).send({ message: e.message });
	}
}

const updateProductId = async (req, res) => {
	const updates = Object.keys(req.body);
	const allowedUpdates = [
		'name',
		'description',
		'quantity',
		'category',
		'approvalDate',
	];
	const isValidOperation = updates.every((update) =>
		allowedUpdates.includes(update)
	);
	if (!isValidOperation) {
		return res.status(400).send({ error: 'Invalid updates' });
	}
	try {
		const product = await Product.findByIdAndUpdate(
			req.params.id,
			req.body,
			{ new: true, runValidator: true }
		);
		if (!product) {
			return res.status(404).send({ message: 'Product does not exist.' });
		}
		res.send(product);
	} catch (e) {
		res.status(400).send({ message: e.message });
	}
}

const deleteProductId = async (req, res) => {
	try {
		const product = await Product.findByIdAndDelete(req.params.id);
		if (!product) {
			return res.status(404).send({ message: 'Product does not exist.' });
		}
		res.send(product);
	} catch (e) {
		res.status(500).send({ message: e.message });
	}
}

const uploadImageProduct = async function (req, res) {
	const file = req.file;
	if (!file) {
		const error = new Error('Please upload a file');
		error.httpStatusCode = 400;
		return next(error);
	}
	res.send({
		filename: file.filename,
		url: 'http://localhost:3000/images/' + file.filename,
	});
	res.send({ message: 'Upload image succesfully.' });
};

export {
	createProduct,
	getProduct,
	getProductId,
	updateProductId,
	deleteProductId,
	uploadImageProduct,
}