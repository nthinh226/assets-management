import { User } from '../models/user.js';
import { sendResetPasswordEmail } from '../emails/accounts.js';
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import sharp from 'sharp';

const createUser = async function (req, res) {
	const user = new User(req.body);
	try {
		await user.save();
		const token = await user.generateAuthToken();
		res.status(201).send({ user, token });
	} catch (e) {
		res.status(400).send({ message: e.message });
	}
};

const getUser = async function (req, res) {
	try {
		const user = await User.find({});
		res.send({ data: user });
	} catch (e) {
		res.status(500).send({ message: e.message });
	}
};

const login = async function (req, res) {
	try {
		const user = await User.findByCredentials(
			req.body.email,
			req.body.password
		);
		const token = await user.generateAuthToken();
		res.send({ user, token });
	} catch (e) {
		res.status(400).send({
			message: 'Please check your email and password',
		});
	}
};

const logout = async function (req, res) {
	try {
		res.send({ message: 'logout succesfully' });
	} catch (e) {
		res.status(500).send({ message: e.message });
	}
};

const forgotPassword = async function (req, res) {
	const email = req.body.email;
	try {
		const user = await User.findOne({ email });
		if (!user) {
			return res
				.status(400)
				.json({ message: 'User with this email does not exist!' });
		} else {
			console.log('Email exists!');
			const token = jwt.sign(
				{ _id: user._id },
				process.env.JWT_FORGOT_PASSWORD_KEY,
				{ expiresIn: '30m' }
			);
			sendResetPasswordEmail(email, token);
			return res.status(200).send({ message: 'Email has been sent!' });
		}
	} catch (error) {
		return res.status(400).send({ message: error.message });
	}
};

const resetPassword = async function (req, res) {
	try {
		const password = req.body.password;
		const token = req.params.token;
		const decoded = jwt.verify(token, process.env.JWT_FORGOT_PASSWORD_KEY);
		const user = await User.findById({
			_id: decoded._id,
		});
		if (!user) {
			return res.status(404).send();
		}
		const newPassword = await bcrypt.hash(password, 8);
		const updateUser = await User.findByIdAndUpdate(
			{ _id: decoded._id },
			{ password: newPassword }
		);
		return res.status(200).send({ token, user: updateUser });
	} catch (e) {
		res.status(404).send({ message: e.message });
	}
};

const getUserId = async function (req, res) {
	try {
		const _id = req.params.id;
		const user = await User.findById(_id);
		if (!user) {
			res.status(404).send();
		}
		res.send(user);
	} catch (e) {
		res.status(500).send({ message: e.message });
	}
};

const updateUserId = async function (req, res) {
	const updates = Object.keys(req.body);
	const allowedUpdates = ['firstName', 'lastName', 'password', 'birthDate'];
	const isValidOperation = updates.every((update) =>
		allowedUpdates.includes(update)
	);
	if (!isValidOperation) {
		return res.status(400).send({ message: 'Invalid updates' });
	}
	try {
		const user = await User.findById(req.params.id);
		updates.forEach((update) => (user[update] = req.body[update]));
		await user.save();

		if (!user) {
			return res.status(404).send({ message: 'User does not exist.' });
		}
		res.send(user);
	} catch (e) {
		res.status(400).send({ message: e.message });
	}
};
const deleteUserId = async function (req, res) {
	try {
		const user = await User.findByIdAndDelete(req.params.id);
		if (!user) {
			return res.status(404).send();
		}
		res.send(user);
	} catch (e) {
		res.status(500).send({ message: e.message });
	}
};

const uploadAvatar = async function (req, res) {
	const buffer = await sharp(req.file.buffer)
		.resize({ width: 250, height: 250 })
		.png()
		.toBuffer();
	req.user.avatar = buffer.toString('base64');
	await req.user.save();
	res.send({ message: 'Upload avatar succesfully.' });
};
const userUploadImage = async function (req, res) {
	const file = req.file;
	if (!file) {
		const error = new Error('Please upload a file');
		error.httpStatusCode = 400;
		return next(error);
	}
	res.send({
		filename: file.filename,
		url: 'http://localhost:3000/images/' + file.filename
	});
	res.send({ message: 'Upload image succesfully.' });
};


export {
	createUser,
	getUser,
	login,
	logout,
	forgotPassword,
	resetPassword,
	getUserId,
	updateUserId,
	deleteUserId,
	uploadAvatar,
	userUploadImage,
};
