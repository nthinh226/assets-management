import mongoose from 'mongoose';

export const connect = async () => {
	try {
		await mongoose.connect(process.env.MONGODB_URL);
		console.log('Connect database succesfully!!!');
	} catch (error) {
		console.log('Connect database failure: ' + error);
	}
};
