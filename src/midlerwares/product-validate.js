import Joi from 'joi';

const productSchema = Joi.object({
	name: Joi.string().trim().required().messages({
		'string.base': `Name should be a type of text`,
		'string.empty': `Name cannot be an empty`,
		'any.required': `Name is a required`,
	}),
	brand: Joi.string().trim().required().messages({
		'string.base': `Brand should be a type of text`,
		'string.empty': `Brand cannot be an empty`,
		'any.required': `Brand is a required`,
	}),
	description: Joi.string(),
	quantity: Joi.number().integer().required().messages({
		'number.base': `Quantity should be a type of number`,
		'any.required': `Quantity is a required`,
	}),
	category: Joi.string().required(),
	approvalDate: Joi.date().required().less('now'),
});

export const productValidation = async (req, res, next) => {
	const payload = {
		name: req.body.name,
		brand: req.body.brand,
		description: req.body.description,
		quantity: req.body.quantity,
		category: req.body.category,
		approvalDate: req.body.approvalDate,
	};

	const { error } = productSchema.validate(payload);
	if (error) {
		return res.status(406).send({ message: error.message });
	} else {
		next();
	}
};
