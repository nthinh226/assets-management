import Joi from 'joi';

const userScheme = Joi.object({
	firstName: Joi.string().alphanum().trim().required().messages({
		'string.base': `First name should be a type of text`,
		'string.empty': `First name cannot be an empty`,
		'any.required': `First name is a required`,
	}),
	lastName: Joi.string().alphanum().trim().required().messages({
		'string.base': `Last name should be a type of text`,
		'string.empty': `Last name cannot be an empty`,
		'string.alphanum': `Last name must only contain alpha-numeric characters`,
		'any.required': `Last name is a required`,
	}),
	birthDate: Joi.date().raw().required().messages({
		'date.base': `Date of Birth should be a type of date`,
		'any.required': `birthDate is a required`,
	}),
	username: Joi.string()
		.alphanum()
		.min(3)
		.max(30)
		.trim()
		.required()
		.messages({
			'string.base': `username should be a type of text`,
			'string.empty': `username cannot be an empty`,
			'string.min': `username should have a minimum length of {#limit}`,
			'any.required': `username is a required`,
		}),
	email: Joi.string()
		.email({
			minDomainSegments: 2,
			tlds: { allow: ['com', 'net'] },
		})
		.trim()
		.messages({
			'string.email': `Email is invalid`,
		}),
	// password: Joi.string().pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')),
	password: Joi.string().min(6).trim().required(),
});

export const userValidation = async (req, res, next) => {
	const payload = {
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		birthDate: req.body.birthDate,
		username: req.body.username,
		email: req.body.email,
		password: req.body.password,
	};

	const { error } = userScheme.validate(payload);
	if (error) {
		return res.status(406).send({ message: error.message });
	} else {
		next();
	}
};
