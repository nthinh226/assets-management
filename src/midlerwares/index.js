import { auth } from './auth.js';
import { productValidation } from './product-validate.js';
import { userValidation } from './user-validate.js';

export const middleware = {
	auth,
	productValidation,
	userValidation,
};
