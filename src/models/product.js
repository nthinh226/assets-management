import mongoose from 'mongoose';

export const Product = mongoose.model('Product', {
	name: {
		type: String,
		required: true,
	},
	description: {
		type: String,
		required: true,
	},
	brand: {
		type: String,
		required: true,
	},
	category: {
		type: String,
		required: true,
	},
	quantity: {
		type: Number,
		required: true,
	},
	approvalDate: {
		type: Date,
		required: true,
	},
});
