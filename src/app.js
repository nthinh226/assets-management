import express from 'express';
import path from 'path';
import { fileURLToPath } from 'url';
import { connect } from './db/db.js';
import { route } from './routes/index.js';
import BodyParser from 'body-parser';

const __fileName = fileURLToPath(import.meta.url);
const __dirName = path.dirname(__fileName);
const publicDirectoryPath = path.join(__dirName, '../public');

const app = express();

//parse application/json and look for raw text
app.use(BodyParser.json());
app.use(BodyParser.urlencoded({ extended: true }));
app.use(BodyParser.text());
app.use(BodyParser.json({ type: 'application/json' }));

app.use(express.json());
app.use(express.static(publicDirectoryPath));

connect(); // Connect to MongoDB
route(app); // Route init

app.get('/images/:id', (req, res)=> {
	res.send()
});
export { app };
