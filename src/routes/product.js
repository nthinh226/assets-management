import express from 'express';
import { middleware } from '../midlerwares/index.js';
import { uploadImage } from '../utils/file-service.js';
import {
	createProduct,
	getProduct,
	getProductId,
	updateProductId,
	deleteProductId,
	uploadImageProduct,
} from '../controllers/product.js';

const router = new express.Router();

router.post(
	'/upload',
	middleware.auth,
	uploadImage.single('image'),
	uploadImageProduct,
	(error, req, res, next) => {
		res.status(400).send({ message: error.message });
	}
);
router.post('/', middleware.productValidation, createProduct);
router.get('/', getProduct);
router.get('/:id', getProductId);
router.patch('/:id', updateProductId);
router.delete('/:id', deleteProductId);

export { router };
