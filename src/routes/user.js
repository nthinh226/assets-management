import express from 'express';
import { middleware } from '../midlerwares/index.js';
import { uploadImage, uploadPhoto } from '../utils/file-service.js';
import {
	createUser,
	getUser,
	login,
	logout,
	forgotPassword,
	resetPassword,
	getUserId,
	updateUserId,
	deleteUserId,
	uploadAvatar,
	userUploadImage,
} from '../controllers/user.js';

const router = new express.Router();

router.post('/', middleware.userValidation, createUser);
router.get('/', getUser);
router.post('/login', login);
router.post('/logout', middleware.auth, logout);
router.put('/forgot-password', forgotPassword);
router.patch('/reset-password/:token', resetPassword);
router.get('/:id', middleware.auth, getUserId);
router.patch('/:id', updateUserId);
router.delete('/:id', deleteUserId);

router.post(
	'/me/avatar',
	middleware.auth,
	uploadPhoto.single('avatar'),
	uploadAvatar,
	(error, req, res, next) => {
		res.status(400).send({ message: error.message });
	}
);
router.post(
	'/me/upload-image',
	middleware.auth,
	uploadImage.single('image'),
	userUploadImage,
	(error, req, res, next) => {
		res.status(400).send({ message: error.message });
	}
);
export { router };
