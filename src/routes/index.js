import { router as productRouter } from './product.js';
import { router as userRouter } from './user.js';

export function route(app) {
	app.use('/products', productRouter);
	app.use('/users', userRouter);
}
